# KV Storage Web UI

[![pipeline status](https://gitlab.com/kv-storage/frontend/badges/master/pipeline.svg)](https://gitlab.com/kv-storage/frontend/commits/master)
[![coverage report](https://gitlab.com/kv-storage/frontend/badges/master/coverage.svg)](https://gitlab.com/kv-storage/frontend/commits/master)

Simple Web UI for KV Storage. Created with Angular 5.1 and Materal Components.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Deploy

To deploy the application to the stage server it is required to commit the changes to the GitLab,
after CI will verify the code does not break the tests, the Test Runner will automatically compile
and bundle the application and push it to the stage server, where it would be placed inside of a docker container.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
