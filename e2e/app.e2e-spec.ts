import { AppPage } from './app.po';

describe('frontend App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Simple KV Storage Web UI');
  });
  it('should display `ADD ENRTY` button', () => {
    page.navigateTo();
    expect(page.getAddEntryButton()).toBeTruthy();
  });
  it('should be able to add new position', () => {
    page.navigateTo();
    page.getAddEntryButton().click();
    page.getKeyNameInputField().sendKeys('__TEST_VAR_NAME');
    page.getValueField().sendKeys('__TEST_VAR_VALUE');
    page.getCardActionsButton();
    expect(page.getEntriesWrapper('__TEST_VAR_NAME').getText()).toContain('__TEST_VAR_NAME');
  });
  it('should be able to edit position', () => {
    page.navigateTo();
    page.getTestEntryEditButton('__TEST_VAR_NAME').click();
    page.getValueField().clear();
    page.getValueField().sendKeys('_');
    page.getCardActionsButton();
    expect(page.getEntriesValueSpan('_').getText()).toBeTruthy();
 });
 it('should be able to open position details', () => {
  page.navigateTo();
  page.getTestEntryDetailsButton('__TEST_VAR_NAME').click();

  expect(page.getEntriesWrapper('__TEST_VAR_NAME').getText()).toBeTruthy();
});
it('should be able to delete position details', () => {
  page.navigateTo();
  page.getTestEntryDeleteButton('__TEST_VAR_NAME').click();
  page.getCardActionsButton();
  expect(page.getEntriesWrapper('__TEST_VAR_NAME').isPresent()).toBeFalsy();
});
});
