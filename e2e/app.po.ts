import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('mat-toolbar p')).getText();
  }
  getAddEntryButton() {
    return element(by.css('.button-wrapper button'));
  }
  getKeyNameInputField() {
    return element(by.css('.key-name input'));
  }
  getValueField() {
    return element(by.css('.value-data input'));
  }
  getCardActionsButton() {
    browser.sleep(500);

    element(by.css('.mat-button')).click();
    browser.sleep(3500);

    return;
  }
  getEntriesWrapper(text) {
    return element(by.cssContainingText('span', text));
  }
  getEntriesValueSpan(text) {
    return element(by.cssContainingText('.entry-value', text));
  }
  getTestEntryEditButton(text) {
    return element(by.cssContainingText('span', text))
      .element(by.xpath('..'))
      .element(by.css('.entry-option:first-child'));
  }
  getTestEntryDetailsButton(text) {
    return element(by.cssContainingText('span', text))
      .element(by.xpath('..'))
      .element(by.css('.entry-value'));
  }
  getTestEntryDeleteButton(text) {
    return element(by.cssContainingText('span', text))
      .element(by.xpath('..'))
      .element(by.css('.entry-option:nth-child(2)'));
  }
}
