import { TestBed, async, inject } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BackendService, StorageItem, EntryDialogComponent } from './shared';
import { HttpClient } from '@angular/common/http';
import { MatDialogModule, MatSnackBarModule, MatDialog } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

const mockItem: StorageItem = { key: 'value' };
const mockStorage: StorageItem = { is: 'test', wat: 'orly' };
export class MockBackendService extends BackendService {
  createKey(item: StorageItem): Observable<StorageItem> {
    return Observable.of(mockItem);
  }
  readKey(item: StorageItem): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve(mockItem.key);
    });
  }
  updateKey(item: StorageItem): Observable<StorageItem> {
    return Observable.of(mockItem);
  }
  deleteKey(item: StorageItem): Observable<StorageItem> {
    return Observable.of(mockItem);
  }
  listStorage(): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve(mockStorage);
    });
  }
}
let dialog: MatDialog;

describe('AppComponent', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, MatDialogModule, MatSnackBarModule, NoopAnimationsModule],
        declarations: [AppComponent],
        providers: [{ provide: BackendService, useClass: MockBackendService }],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );
  beforeEach(
    inject([MatDialog], (d: MatDialog) => {
      dialog = d;
    })
  );

  it(
    'should create the app',
    async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    })
  );
  it(
    `should have as title 'Simple KV Storage Web UI' in nav bar`,
    async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('mat-toolbar p').textContent).toContain('Simple KV Storage Web UI');
    })
  );
  it(
    `should return list of current entries in the storage at initiation`,
    async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const component = fixture.componentInstance;
      component.ngOnInit().then(() => {
        fixture.detectChanges();

        expect(component.storage).toEqual([{ key: 'is', value: 'test' }, { key: 'wat', value: 'orly' }]);
      });
    })
  );
  it(
    `should return a key value of the selected entry of the storage`,
    async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const component = fixture.componentInstance;
      component.getKeyValue(mockItem).then(result => {
        fixture.detectChanges();

        expect(result).toEqual('value');
      });
    })
  );
});
