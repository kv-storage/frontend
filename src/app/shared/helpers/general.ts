import { KVStorage, StorageItem } from '../types';

// Helper function to convert Map from
// KV Storage to array, so it could be looped over
// (for frontend representation of Storage only)
export const convertToArray = (storage): KVStorage => {
  return Object.entries(storage).map((entry: StorageItem) => {
    return {
      key: entry[0],
      value: entry[1]
    };
  });
};
