import { MatDialog, MatDialogModule, MatCardModule } from '@angular/material';
import { NgModule } from '@angular/core';
import {  DetailsDialogComponent } from '..';
import { TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [MatCardModule, MatDialogModule, CommonModule, NoopAnimationsModule],
  declarations: [DetailsDialogComponent],
  entryComponents: [DetailsDialogComponent],
  exports: [DetailsDialogComponent]
})
class TestModule {}

describe('DetailsDialogComponent', () => {
  let component: DetailsDialogComponent;
  let dialog: MatDialog;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
    });
  });

  beforeEach(() => {
    dialog = TestBed.get(MatDialog);
    const dialogRef = dialog.open(DetailsDialogComponent);

    component = dialogRef.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should return `string` after initiation on the provided mock data', () => {
    component.data = 'test string';
    const result = component.ngOnInit();

    expect(component.valueType).toEqual('string');
  });
  it('should return `number` after initiation on the provided mock data', () => {
    component.data = 123;
    const result = component.ngOnInit();

    expect(component.valueType).toEqual('number');
  });
  it('should return `boolean` after initiation on the provided mock data', () => {
    component.data = false;
    const result = component.ngOnInit();

    expect(component.valueType).toEqual('boolean');
  });
});
