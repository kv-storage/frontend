import { MatDialog, MatDialogModule, MatCardModule, MatDialogRef } from '@angular/material';
import { NgModule } from '@angular/core';
import { ConfirmationDialogComponent } from '..';
import { TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [MatCardModule, MatDialogModule, CommonModule, NoopAnimationsModule],
  declarations: [ConfirmationDialogComponent],
  entryComponents: [ConfirmationDialogComponent],
  exports: [ConfirmationDialogComponent]
})
class TestModule {}

describe('ConfirmationDialogComponent', () => {
  let component: ConfirmationDialogComponent;
  let dialog: MatDialog;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule]
    });
  });

  beforeEach(() => {
    dialog = TestBed.get(MatDialog);
    const dialogRef = dialog.open(ConfirmationDialogComponent);

    component = dialogRef.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should return `true` to the parent component', () => {
    const result = component.deleteEntry();

    expect(result).toEqual(true);
  });
});
