import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSlideToggleChange } from '@angular/material';
import { convertToArray } from '../../helpers/general';
import { StorageItem } from '../..';

@Component({
  selector: 'app-entry-dialog',
  templateUrl: './entry-dialog.component.html',
  styleUrls: ['./entry-dialog.component.scss']
})
export class EntryDialogComponent implements OnInit {
  availableTypes = ['string', 'boolean', 'number'];
  enrtyPositionName = '';
  entryPositionValue: string | boolean | number;
  valueType: string;

  constructor(
    public dialogRef: MatDialogRef<EntryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: StorageItem
  ) {}

  // If it is *Edit* dialog, preload the form with values of
  // the current entry, assign the type of the value to the `valueType`
  ngOnInit() {
    if (this.data) {
      console.log(this.data);
      this.enrtyPositionName = this.data.key;
      this.entryPositionValue = this.data.value;
      this.valueType = typeof this.entryPositionValue;
    }
  }

  // If user clicked *add* or *update*, create new object
  // representing the entry, return it to the parent component
  assignEntry() {
    const enrtyObject = {};
    enrtyObject[this.enrtyPositionName] = this.entryPositionValue;
    return enrtyObject;
  }

  // Bind value of toggle with `entryPositionValue` variable
  booleanChange(event: MatSlideToggleChange) {
    this.entryPositionValue = event.checked;
  }
  // If user picks `boolean` as the value type,
  // initiate it with value `false`
  selectedTypeChange(event: string) {
    if (event === 'boolean') {
      this.entryPositionValue = false;
    } else {
      this.entryPositionValue = undefined;
    }
  }
}
