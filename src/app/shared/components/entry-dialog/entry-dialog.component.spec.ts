import {
  MatDialog,
  MatDialogModule,
  MatCardModule,
  MatInputModule,
  MatSelectModule,
  MatSlideToggleModule
} from '@angular/material';
import { NgModule } from '@angular/core';
import { EntryDialogComponent, StorageItem } from '../..';
import { TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@NgModule({
  imports: [
    MatCardModule,
    MatDialogModule,
    CommonModule,
    NoopAnimationsModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule
  ],
  declarations: [EntryDialogComponent],
  entryComponents: [EntryDialogComponent],
  exports: [EntryDialogComponent]
})
class TestModule {}

describe('EntryDialogComponent', () => {
  let component: EntryDialogComponent;
  let dialog: MatDialog;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    dialog = TestBed.get(MatDialog);
    const dialogRef = dialog.open(EntryDialogComponent);

    component = dialogRef.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should assign local variables with provided data if it is *Edit* dialog', () => {
    component.data = { name: 'test' };
    component.ngOnInit();
    Observable.timer(100).subscribe(() => {
      expect(component.enrtyPositionName).toEqual('name');
      expect(component.entryPositionValue).toEqual('test');
    });
  });

});
