// Representation of the single
// KV Storage Entry
export interface StorageItem {
  [key: string]: any;
}
// Representation of the entire KV Storage
// converted to array
export interface KVStorage {
  [key: string]: any;
  length: number;
  push(item: StorageItem): void;
}
