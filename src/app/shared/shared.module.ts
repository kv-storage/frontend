import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from './material.module';
import { BackendService } from './services';
import {
  EntryDialogComponent,
  ConfirmationDialogComponent,
  DetailsDialogComponent
} from './components';

// Array of all the Dialog components
const ENTRY_COMPONENTS = [
  EntryDialogComponent,
  ConfirmationDialogComponent,
  DetailsDialogComponent
];
@NgModule({
  imports: [CommonModule, MaterialModule, HttpClientModule, FormsModule],
  exports: [MaterialModule],
  declarations: ENTRY_COMPONENTS,
  entryComponents: ENTRY_COMPONENTS,
  providers: [BackendService]
})
export class SharedModule {}
