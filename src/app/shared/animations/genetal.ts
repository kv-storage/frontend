import { trigger, state, style, transition, animate } from '@angular/animations';

// Animations for smooth transition of new/deleted entries
export const newPositionAnimation = trigger('newPositionAnimation', [
  transition(':enter', [
    style({ transform: 'translateY(20%)', opacity: 0 }),
    animate('100ms', style({ transform: 'translateY(0)', opacity: 1 }))
  ]),
  transition(':leave', [
    style({ transform: 'translateY(0)', opacity: 1 }),
    animate('100ms', style({ transform: 'translateY(20%)', opacity: 0 }))
  ])
]);
