import { TestBed, inject } from '@angular/core/testing';

import { BackendService } from './backend.service';
import { MockBackendService } from '../../app.component.spec';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BackendService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: BackendService, useClass: MockBackendService }]
    });
  });

  it('should be created', inject([BackendService], (service: BackendService) => {
    expect(service).toBeTruthy();
  }));
});
