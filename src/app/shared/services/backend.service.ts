import { Injectable } from '@angular/core';
import { StorageItem } from '../types';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class BackendService {
  // Get current location of the backend from ENV variable
  backnend = environment.backendURL;

  constructor(private http: HttpClient) {}

  // Create new Entry in the KV Storage
  // Creates a *POST* request to the backend
  // Returns Observable, has to be handled in the component
  createKey(item: StorageItem): Observable<StorageItem> {
    return this.http.post<StorageItem>(`${this.backnend}/createKey`, item);
  }

  // Get entry's value in the KV Storage
  // Creates a *GET* request to the backend
  // Returns Promise with JSON.stringify'ed `responce` or `error`
  readKey(item: StorageItem): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get<StorageItem>(`${this.backnend}/readKey/${item.key}`)
        .subscribe(responce => resolve(responce), error => reject(error));
    });
  }

  // Update entry's value in the KV Storage
  // Creates a *PUT* request to the backend
  // Returns Observable, has to be handled in the component
  updateKey(item: StorageItem): Observable<StorageItem> {
    return this.http.put<StorageItem>(`${this.backnend}/updateKey`, item);
  }

  // Delete entry in the KV Storage
  // Creates a *PUT* request to the backend
  // Returns Observable, has to be handled in the component
  deleteKey(item: StorageItem): Observable<StorageItem> {
    return this.http.delete<StorageItem>(
      `${this.backnend}/deleteKey/${item.key}`
    );
  }

  // Get the entire KV Storage
  // Creates a *GET* request to the backend
  // Returns Promise with JSON.stringify'ed `responce` or `error`
  listStorage(): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.http
        .get<StorageItem>(`${this.backnend}/listStorage`)
        .subscribe(responce => resolve(responce), error => reject(error));
    });
  }
}
