import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';

import { BackendService } from './shared/services';
import { KVStorage, StorageItem } from './shared/types';
import { convertToArray } from './shared/helpers';
import { newPositionAnimation } from './shared/animations';
import { EntryDialogComponent, ConfirmationDialogComponent, DetailsDialogComponent } from './shared/components';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [newPositionAnimation]
})
export class AppComponent implements OnInit {
  storage: KVStorage = [];

  constructor(private bs: BackendService, public dialog: MatDialog, public snackBar: MatSnackBar) {}

  // Load all entries of the KV Storage on page initiation
  async ngOnInit() {
    this.getKVStorage();
  }

  // Add new entry to the KV Storage
  // Create a form dialog, and if an entry was created, submit it to the server
  // If succeed, update the view with latest changes, else display error message
  addEntry(): void {
    const dialogRef = this.dialog.open(EntryDialogComponent, {});

    dialogRef.afterClosed().subscribe((item: StorageItem) => {
      if (item) {
        this.bs.createKey(item).subscribe(
          responce => {
            this.getKVStorage();
            this.showStatus('Succesfuly added the entry to the storage');
          },
          (error: HttpErrorResponse) => {
            if (error.status === 406) {
              this.showStatus(
                `This key already exists in the KV Storage. Use different key or try the *Update* button`
              );
            } else {
              this.showStatus('Failed to add the entry to the storage, check the console for details');
            }
          }
        );
      }
    });
  }

  // Edit Exisiting KV Storage Entry
  // Create a form dialog, pass to it data with current entry
  // After the user gave new value of the entry, submit it to the server
  // If succeed, update the view with latest changes, else display error message
  editEntry(entry: StorageItem): void {
    const dialogRef = this.dialog.open(EntryDialogComponent, {
      data: entry
    });

    dialogRef.afterClosed().subscribe((modifyedItem: StorageItem) => {
      if (modifyedItem) {
        this.bs.updateKey(modifyedItem).subscribe(
          result => {
            this.getKVStorage();
            this.showStatus('Succesfuly updated the enrty');
          },
          (error: HttpErrorResponse) => {
            this.showStatus('Failed to update the enrty, check the console for details');
          }
        );
      }
    });
  }

  // Get Value of Exisiting Entry
  // Create a form dialog, pass to it data with server responce
  async detailedEntry(entry: StorageItem) {
    const dialogRef = this.dialog.open(DetailsDialogComponent, {
      data: await this.getKeyValue(entry)
    });
  }

  // Delete Exisiting Entry
  // Create a dialog with the entry in question
  // After confirmation, delete the enrty from the server,
  // If succeed, update view and display message,
  // else display an error message
  deleteEntry(entry: StorageItem): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: entry
    });

    dialogRef.afterClosed().subscribe((confirmation: boolean) => {
      if (confirmation) {
        this.bs.deleteKey(entry).subscribe(
          responce => {
            this.getKVStorage();
            this.showStatus('Succesfuly deleted position');
          },
          (error: HttpErrorResponse) => {
            this.showStatus('Failed to add the entry to the storage, check console for details');
          }
        );
      }
    });
  }

  // Create small snackbar with status of current server call
  // Display it for 3.5s
  showStatus(message: string): void {
    this.snackBar.open(message, null, {
      duration: 3500
    });
  }

  // Function for comparison current set of entrties with one on
  // the server, distinct by the entry key
  trackByFn(index: number, enrty: StorageItem) {
    return enrty.key;
  }

  // Get All the Entries of the KV Strorage
  // After that, convert them from Map  `{key: value}` to an Array of
  // {key, value}, so it would be possible to create a loop on them
  async getKVStorage() {
    this.storage = convertToArray(await this.bs.listStorage());
  }
  // Get Value of the the Entry, found by the `key`
  // and return it
  async getKeyValue(enrty: StorageItem) {
    return await this.bs.readKey(enrty);
  }
}
